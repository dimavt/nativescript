# docker nativescript
Контейнер для билда android native script проектов.


В контейнере подготовлены:
- [NativeScript](https://www.nativescript.org/) (3.x)
- [Android SDK Tools](https://developer.android.com/) (25.2.5)
- [Android SDK Platform](https://developer.android.com/) (25)
- [NodeJS](https://nodejs.org/) (9.x)
- [Java](https://www.java.com/) (1.8.0)
- [Ubuntu](https://www.ubuntu.com/) (16.04 LTS)
# Запуск

```
# (опционально) собираем из исходников
docker build -t registry.gitlab.com/dimavt/nativescript .
# Добавляем алиас для команты tns
alias tns='docker run -it --rm  -v $PWD:/app registry.gitlab.com/dimavt/nativescript:latest tns'
# При использовании linux если передть флаг --priviliged , станет доступен запуск на подключенном для дебага девайсе
```


# Использование
```
cd project
tns build android
```

# Настройка


